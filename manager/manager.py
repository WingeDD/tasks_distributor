#!/usr/bin/python3

import sys
import json
import uuid
import pika
from pika.adapters.asyncio_connection import AsyncioConnection
import grpc
import exec_pb2
import exec_pb2_grpc
import asyncio


LISTEN_ADDR: str = '[::]:50051'
RMQ_HOST: str = "rabbitmq"
RMQ_LOGIN: str = "fwd"
RMQ_PW: str = "123456"


ENTRY_POINT_KEY: str = "entry_point"
CODE_KEY: str = "code"
ARGUMENTS_KEY: str = "arguments"


TASKS_QUEUE: str = "tasks"
RESULTS_QUEUE: str = "results"


class ExecMQClient(object):
    # async def wait_channel(self):
    #     while True:
    #         if self.__channel is None:
    #             await asyncio.sleep(0.02)
    #         else:
    #             break

    def on_channel_open(self, channel):
        self.__channel = channel

    def on_connection_open(self, _unused_connection):
        self.__connection.channel(on_open_callback=self.on_channel_open)

    @classmethod
    async def create(
        cls, host: str, credentials: pika.PlainCredentials, requests_routing_key: str = TASKS_QUEUE, responses_queue: str = RESULTS_QUEUE
    ) -> "ExecMQClient":
        self = cls()
        self.__requests_routing_key = requests_routing_key
        self.__responses_queue = responses_queue
        self.__responses = dict() # corr_id: res
        self.__channel = None
        self.__connection = await AsyncioConnection(
            pika.ConnectionParameters(host=host, credentials=credentials),
            on_open_callback=self.on_connection_open,
            on_open_error_callback=lambda x, y: exit(1)
        )
        # await self.wait_channel()
        self.__channel.queue_declare(queue=self.__responses_queue, exclusive=True)
        return self

    async def __aexit__(self, *excinfo):
        self.__channel.close()
        self.__connection.close()

    def on_response(self, ch, method, props, body):
        if props.correlation_id in self.__responses:
            self.__responses[props.correlation_id] = body

    async def wait_res(self, corr_id: str) -> str:
        while True:
            if self.__responses[corr_id] is None:
                await asyncio.sleep(0.02)
            else:
                res: str = self.__responses[corr_id]
                del self.__responses[corr_id]
                return res

    async def call(self, entry_point: str, arguments: str, code: str):
        corr_id = str(uuid.uuid4())
        while True:
            if corr_id in self.__responses:
                corr_id = str(uuid.uuid4())
            else:
                self.__responses[corr_id] = None
                break

        self.__channel.basic_consume(
            queue=self.__responses_queue,
            on_message_callback=self.on_response,
            auto_ack=True
        )

        self.__channel.basic_publish(
            exchange='',
            routing_key=self.__requests_routing_key,
            properties=pika.BasicProperties(
                correlation_id=corr_id,
            ),
            body=json.dumps({ENTRY_POINT_KEY: entry_point, ARGUMENTS_KEY: arguments, CODE_KEY: code})
        )

        return await self.wait_res(corr_id)


class Exec(exec_pb2_grpc.ExecServicer):
    @classmethod
    async def create(cls, rmq_host, rmq_credentials) -> "Exec":
        self = cls()
        self.__exec_mq_client = await ExecMQClient.create(rmq_host, rmq_credentials)
        return self

    async def FunctionExec(self, request: exec_pb2.ExecInput, context: grpc.aio.ServicerContext) -> exec_pb2.ExecOutput:
        return exec_pb2.ExecOutput(result=await self.__exec_mq_client.call(request.entry_point, request.arguments, request.code))



async def serve(listen_addr: str, rmq_host: str, rmq_credentials: pika.PlainCredentials) -> None:
    server: grpc.aio.Server = grpc.aio.server()
    exec_pb2_grpc.add_ExecServicer_to_server(await Exec.create(rmq_host, rmq_credentials), server)
    server.add_insecure_port(listen_addr)
    await server.start()
    await server.wait_for_termination()


if __name__ == '__main__':
    asyncio.run(serve(LISTEN_ADDR, RMQ_HOST, pika.PlainCredentials(RMQ_LOGIN, RMQ_PW)))
