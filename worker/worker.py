#!/usr/bin/python3

import json
import pika


HOST: str = "rabbitmq"
LOGIN: str = "fwd"
PW: str = "123456"

TASKS_QUEUE: str = "tasks"
RESULTS_QUEUE: str = "results"


ENTRY_POINT_KEY: str = "entry_point"
CODE_KEY: str = "code"
ARGUMENTS_KEY: str = "arguments"

LOC_RES_VAR: str = "res"
DEL_OP: str = "del"
RESULT_OUTPUT_KEY: str = "result"
EXCEPTION_OUTPUT_KEY: str = "exception"


def on_request(
    ch, method, props, body,
    queue
):
    call_params: dict = json.loads(body)
    try:
        locals_dict = {}
        exec(
            f"{call_params[CODE_KEY]}\n{LOC_RES_VAR} = {call_params[ENTRY_POINT_KEY]}({call_params[ARGUMENTS_KEY]})\n{DEL_OP} {call_params[ENTRY_POINT_KEY]}",
            {}, locals_dict
        )
        response = json.dumps({RESULT_OUTPUT_KEY: locals_dict[LOC_RES_VAR], EXCEPTION_OUTPUT_KEY: None})
    except Exception as e:
        response = json.dumps({RESULT_OUTPUT_KEY: None, EXCEPTION_OUTPUT_KEY: f"{repr(e)}"})

    ch.basic_publish(
        exchange='',
        routing_key=queue,
        properties=pika.BasicProperties(correlation_id = props.correlation_id),
        body=response
    )
    ch.basic_ack(delivery_tag=method.delivery_tag)


def start_working(host: str, credentials: pika.PlainCredentials, tasks_queue: str = TASKS_QUEUE, results_queue: str = RESULTS_QUEUE):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=host, credentials=credentials)
    )
    channel = connection.channel()
    channel.queue_declare(queue=tasks_queue)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=tasks_queue, on_message_callback=lambda a, b, c, d: on_request(a, b, c, d, results_queue))
    channel.start_consuming()


if __name__ == '__main__':
    start_working(HOST, pika.PlainCredentials(LOGIN, PW))
