from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class ExecInput(_message.Message):
    __slots__ = ["arguments", "code", "entry_point"]
    ARGUMENTS_FIELD_NUMBER: _ClassVar[int]
    CODE_FIELD_NUMBER: _ClassVar[int]
    ENTRY_POINT_FIELD_NUMBER: _ClassVar[int]
    arguments: str
    code: str
    entry_point: str
    def __init__(self, entry_point: _Optional[str] = ..., arguments: _Optional[str] = ..., code: _Optional[str] = ...) -> None: ...

class ExecOutput(_message.Message):
    __slots__ = ["result"]
    RESULT_FIELD_NUMBER: _ClassVar[int]
    result: str
    def __init__(self, result: _Optional[str] = ...) -> None: ...
