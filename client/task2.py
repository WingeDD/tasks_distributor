#!/usr/bin/python3

import asyncio
import client as cl


TASKS_COUNT: int = 10
MANAGER_ADDR: str = "localhost:50051"


def job_for_worker(sleep_time: float, tag: str):
    import time
    time.sleep(sleep_time)
    return f"#{tag} slept for {sleep_time} seconds"


async def launch():
    rpc_client = cl.RemoteExecAsync(MANAGER_ADDR)
    tasks = [None] * TASKS_COUNT
    for idx in range(TASKS_COUNT):
        task = asyncio.create_task(rpc_client("job_for_worker", f"1, {idx}", job_for_worker), name=f"#{idx}")
        task.add_done_callback(lambda x: print(x.result()))
        tasks[idx] = task
    futures = await asyncio.gather(*tasks)
    print("\n")
    print(futures)

if __name__ == '__main__':
    asyncio.run(launch())
