#!/usr/bin/python3

from sys import argv
import client as cl


MANAGER_ADDR: str = "localhost:50051"


def job_for_worker(t: float, key_sleep: str):
    import time
    time.sleep(t)
    return {key_sleep: t}

job_for_worker_s: str = """def job_for_worker(t: float, key_sleep: str):
    import time
    time.sleep(t)
    return {key_sleep: t}
"""


if __name__ == '__main__':
    arg = float(argv[1]) if len(argv) == 2 else 1.5
    rpc_client = cl.RemoteExec(MANAGER_ADDR)
    res = rpc_client("job_for_worker", f"{arg}, 'slept_for'", job_for_worker_s)
    print(res)
