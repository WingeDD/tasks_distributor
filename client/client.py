#!/usr/bin/python3

from typing import Callable, Any
import grpc
import inspect
import exec_pb2
import exec_pb2_grpc


class RemoteExec:
    def __init__(self, target: str) -> None:
        self.__chan = grpc.insecure_channel(target)
        self.__stub = exec_pb2_grpc.ExecStub(self.__chan)

    def __del__(self) -> None:
        self.__chan.close()

    def __call__(self, entry_point: str, args: str, func: str | Callable) -> str:
        if type(func) != str:
            func = inspect.getsource(func)
        response = self.__stub.FunctionExec(exec_pb2.ExecInput(entry_point=entry_point, arguments=args, code=func))
        return response.result


class RemoteExecAsync:
    def __init__(self, target: str) -> None:
        self.__chan = grpc.aio.insecure_channel(target)
        self.__stub = exec_pb2_grpc.ExecStub(self.__chan)

    async def __aexit__(self, *excinfo):
        await self.__chan.close()

    async def __call__(self, entry_point: str, args: str, func: str | Callable) -> str:
        if type(func) != str:
            func = inspect.getsource(func)
        response = await self.__stub.FunctionExec(exec_pb2.ExecInput(entry_point=entry_point, arguments=args, code=func))
        return response.result
